# frozen_string_literal: true

RSpec.describe Strum::Pipeline do
  it "has a version number" do
    expect(Strum::Pipeline::VERSION).not_to be nil
  end
end
