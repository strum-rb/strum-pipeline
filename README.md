# Strum::Pipeline

Welcome to your new gem! In this directory, you'll find the files you need to be able to package up your Ruby library into a gem. Put your Ruby code in the file `lib/strum/pipeline`. To experiment with that code, run `bin/console` for an interactive prompt.

TODO: Delete this and the text above, and describe your gem

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'strum-pipeline'
```

And then execute:

    $ bundle install

Or install it yourself as:

    $ gem install strum-pipeline

## Usage

TODO: Write usage instructions here

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake spec` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on GitHub at https://github.com/[USERNAME]/strum-pipeline.

## Example

```ruby
# frozen_string_literal: true

require "sneakers/runner"
require "strum/esb"
require "strum/pipeline"

# demo class
class DemoPipeline
  include Strum::Pipeline

  # changing default exchangers
  Strum::Esb.config.info_exchange = "demo.info"
  Strum::Esb.config.event_exchange = "demo.events"
  Strum::Esb.config.action_exchange = "demo.actions"
  Strum::Esb.config.notice_exchange = "demo.notice"

  # changing default redis connection
  Strum::Pipeline.config.redis_connection = { url: ENV.fetch("DEMO_REDIS_URL") }

  ttl 60
  init :event, "identity/create"
  init :event, "identity/update"
  step :action, "get/user", :demo_custom_handler
  step :action, "get/company"
  step :info, "user"

  def event_identity_create(_payload)
    storage[:counter1] = 1
    storage[:counter] = 1
    Strum::Esb::Action.call({}, :get, :user)
    # Strum::Esb::Action.({}, :get, :company)
  end

  def demo_custom_handler(_payload)
    Strum::Esb::Info.call({}, :user)
    storage[:counter2] = 2
    Strum::Pipeline::Storage[:counter] = 2
    Strum::Pipeline::Storage.list << 2
  end

  def info_user(payload)
    logger.debug payload
    storage[:counter3] = 3
    storage[:counter] = 3
  end
end

r = Sneakers::Runner.new([DemoPipeline])
r.run
```

Also Redis connection can be set up by env var:
```
PIPELINE_REDIS_HOST=127.0.0.1
PIPELINE_REDIS_PORT=6379
PIPELINE_REDIS_DB=0
```

