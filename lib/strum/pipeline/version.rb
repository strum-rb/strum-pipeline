# frozen_string_literal: true

module Strum
  module Pipeline
    VERSION = "0.3.0"
  end
end
